<?php
require_once "config.php";

    $user_empid     = mysqli_real_escape_string($link, $_POST['empid']);
    $user_name     = mysqli_real_escape_string($link, $_POST['name']);
    $user_loc     = mysqli_real_escape_string($link, $_POST['location']);
    
    if((trim($user_empid) == '') || (trim($user_name)=='') || (trim($user_loc)==''))
    {
        echo 'Please enter all details';
    }
    else{
        $query="select * from tbl_users where emp_id ='$user_empid' and eventname='$event_name'";
        $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
        //echo $query;
        if (mysqli_affected_rows($link) > 0) 
        {
            $row = mysqli_fetch_row($res); 
                
            $login_date   = date('Y/m/d H:i:s');
            $logout_date   = date('Y/m/d H:i:s', time() + 30);
            
            try{
                $today=date("Y/m/d H:i:s");
        
                $dateTimestamp1 = strtotime($row[6]);
                $dateTimestamp2 = strtotime($today);
                //echo $row[5];
                if ($dateTimestamp1 > $dateTimestamp2)
                {
                  echo "-1";
                }
                else
                {
                  $login_date   = date('Y/m/d H:i:s');
                  $logout_date   = date('Y/m/d H:i:s', time() + 30);
    
                  $query="UPDATE tbl_users set login_date='$login_date', logout_date='$logout_date', logout_status='1' where emp_id='$user_empid' and eventname='$event_name'";
                  $res = mysqli_query($link, $query) or die(mysqli_error($link));
                  
                  $_SESSION['user_empid']    = $row[2];
                  $_SESSION['user_name']     = $row[1];
                  $_SESSION["user_id"]  = $row[0];
                  $_SESSION["user_loc"]  = $row[3];
                  
                  echo "s";
                }
            }
            catch(PDOException $e){
              echo $e->getMessage();
            }
                
            
        }
        else{
            //echo "0";
            $login_date   = date('Y/m/d H:i:s');
            $logout_date   = date('Y/m/d H:i:s', time() + 30);
            $query="insert into tbl_users(emp_name, emp_id, emp_location, login_date, logout_date, reg_date, logout_status, eventname) values('$user_name','$user_empid','$user_loc','$login_date','$logout_date','$login_date','1','$event_name')";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $reg_id = mysqli_insert_id($link);
            if($reg_id > 0 )
            {
                  $_SESSION['user_empid']    = $user_empid;
                  $_SESSION['user_name']     = $user_name;
                  $_SESSION['user_loc']     = $user_loc;
                echo "s";
            }
            else{
                echo "Please try again.";
            }
        }
        
    }

?>
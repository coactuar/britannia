<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_empid"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_empid=$_SESSION["user_empid"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where emp_id='$user_empid'  and eventname='$event_name'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_empid"]);
            unset($_SESSION["user_loc"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Britannia :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid main">
    <div class="row header">
        <div class="col-12 col-md-2">
            <img src="img/logo.png" class="img-fluid logo" alt=""/> 
        </div>
        
    </div>
    <div class="row login-info">
        <div class="col-12 text-right">
          Hello, <?php echo $_SESSION['user_name']; ?>! <a class="logout" href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12 col-md-8 col-lg-6 offset-lg-1">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
            <div id="question" class="mb-5">
                <div id="question-form" class="panel panel-default">
                    <form method="POST" action="#" class="form panel-body" role="form">
                        <div class="row">
                            <div class="col-12">
                            <div id="ques-message"></div>
                            <div class="form-group">
                                <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="5"></textarea>
                            </div>
                            
                            </div>
                            <div class="col-12">
                            <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                            <input type="hidden" id="user_email" name="user_email" value="<?php echo $_SESSION['user_empid']; ?>">
                            <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                            </div>
                        </div>
                  </form>
                </div>
            </div>
        
        </div>

    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);


</script>

</body>
</html>
-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 11:14 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coacteh9_britannia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`) VALUES
(1, 'Aditya', 'axaditya@gmail.com', 'Hi', '2020-05-23 13:31:51', 'britannia02'),
(2, 'asdga qasf', 'dsfasf@dfadf.com', 'fgdf dsg dsfg', '2020-05-23 17:26:38', 'britannia02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `emp_location` varchar(100) NOT NULL,
  `reg_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0',
  `eventname` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `emp_name`, `emp_id`, `emp_location`, `reg_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(2, 'test', 'test@test.com', 'prestige', '2020-01-10 11:58:50', '2020-03-26 13:37:49', '2020-03-26 14:58:49', 1, 'britannia'),
(3, 'Sagar Chakraborty', 'schakz@yahoo.com', 'Kolkata', '2020-01-10 16:57:44', '2020-01-10 17:02:30', '2020-01-10 17:11:30', 1, 'britannia'),
(4, 'Pooja', 'pooja@coact.co.in', 'Mumbai ', '2020-02-06 17:44:27', '2020-02-06 17:44:27', '2020-02-06 17:44:57', 1, 'britannia'),
(5, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'PUNE', '2020-03-26 12:20:17', '2020-03-27 08:55:08', '2020-03-27 09:27:38', 1, 'britannia'),
(6, 'Sujatha ', 'sujatha@coact.co.in', 'Bangalore ', '2020-03-26 16:41:50', '2020-03-26 16:41:50', '2020-03-26 16:42:20', 1, 'britannia'),
(7, 'CHETAN', 'chetan@coact.co.in', 'Pune', '2020-03-27 09:16:46', '2020-03-27 09:16:46', '2020-03-27 11:03:22', 1, 'britannia'),
(8, 'Akshat', 'akshatjharia@gmail.com', 'Bangalore', '2020-05-23 12:53:17', '2020-05-23 12:53:17', '2020-05-23 13:01:18', 1, 'britannia'),
(9, 'Aditya', 'axaditya@gmail.com', 'Lucknow', '2020-05-23 13:26:43', '2020-05-23 13:26:43', '2020-05-23 13:28:14', 1, 'britannia'),
(10, 'Aditya', 'axaditya@gmail.com', 'Lucknow', '2020-05-23 13:31:36', '2020-05-23 18:06:26', '2020-05-23 18:10:58', 1, 'britannia02'),
(11, 'Akshat', 'akshat@coact.co.in', 'Bangalore', '2020-05-23 13:33:54', '2020-06-02 11:21:12', '2020-06-02 15:57:24', 1, 'britannia02'),
(12, 'v', 'v@c.m', 'pune', '2020-05-23 15:01:44', '2020-05-23 15:01:44', '2020-05-23 15:03:44', 1, 'britannia02'),
(13, 'asdga qasf', 'dsfasf@dfadf.com', 'sfdsfasdf', '2020-05-23 17:26:29', '2020-05-23 17:26:29', '2020-05-23 17:48:00', 1, 'britannia02'),
(14, 'Akshat', 'akshatjharia@gmail.com', 'bangalore', '2020-05-23 17:53:41', '2021-09-14 11:14:58', '2021-09-14 11:27:28', 1, 'britannia02'),
(15, 'luttappi', 'luttappi@ymail.com', 'Kochi', '2020-05-26 11:38:26', '2020-05-26 11:38:26', '2020-05-26 13:38:27', 1, 'britannia02'),
(16, 'a', 'a@s.d', 'c', '2020-05-27 10:08:08', '2020-05-27 10:08:08', '2020-05-27 10:21:39', 1, 'britannia02'),
(17, 'Kiran', 'kiran.chougule@saluto.in', 'Thane', '2020-05-28 10:18:00', '2020-05-28 10:18:00', '2020-05-28 22:46:14', 1, 'britannia02'),
(18, 'Ravi Khot', 'v@c.n', 'P', '2020-05-30 10:04:44', '2020-05-30 10:04:44', '2020-05-30 10:06:15', 1, 'britannia02'),
(19, 'Pooja', 'pooja@coact.co.in', 'Mumbai', '2020-06-01 11:20:36', '2021-08-18 15:09:24', '2021-08-18 16:56:25', 1, 'britannia02'),
(20, 'Sujatha ', 'sujatha@coact.co.in', 'Bangalore ', '2020-06-01 11:21:18', '2020-06-02 13:16:51', '2020-06-02 13:17:51', 1, 'britannia02'),
(21, 'Pawan ', 'p@gmail.com', 'Mumbai ', '2020-06-01 11:23:28', '2020-06-02 15:14:07', '2020-06-02 15:14:37', 1, 'britannia02'),
(22, 'PAWAN', 'pawan@coact.co.in', 'Bangalore', '2020-06-01 11:57:26', '2021-08-14 15:42:31', '2021-08-14 15:43:01', 1, 'britannia02'),
(23, 'suhas', 'suhas@kikr.com', 'Bangalore', '2021-06-21 11:16:29', '2021-06-21 11:16:29', '2021-06-21 11:17:29', 1, 'britannia02'),
(24, 'pooja', 'pooja@oact.co.n', 'Mumbai', '2021-08-14 15:42:27', '2021-08-14 15:42:27', '2021-08-14 16:10:01', 1, 'britannia02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
